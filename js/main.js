const BASE_URL = "https://63b2c9c35901da0ab36dc13c.mockapi.io";

function renderFoodList(food){
    var contentHTML = "";

food.reverse(). forEach(function(item){
    var contentTr= `  <tr>
                        <td>${item.maMon}</td>
                        <td>${item.tenMon}</td>
                        <td>${item.giaMon}</td>
                        <td>${
                            item.loaiMon
                                ? "<span class= 'text-primary'>Mặn</span>"
                                : " <span class='text-success'>chay</span>  "
                        }</td>
                        <td>${convertString(50, item.hinhAnh)}</td>
                        <td><button onclick="xoaMonAn('${item.maMon}')" class="btn btn-danger">Xoá</button>
                        <button onclick="suaMonAn('${item.maMon}')" class="btn btn-warning">Sửa</button></td>
                        </tr>`;
    contentHTML += contentTr;
});



document.getElementById("tbodyFood").innerHTML = contentHTML;


}

function fetchFoodList(){
   
batLoading();


axios({
url: `${BASE_URL}/food`,
method: "GET",
}).then(function(res){
    tatLoading();
console.log("🚀 ~ file: main.js:29 ~ res", res);
var foodList = res.data;
renderFoodList(foodList);

}).catch(function(err){
    tatLoading();
    console.log("🚀 ~ file: main.js:35 ~ err", err);
    
});
}

fetchFoodList();
function xoaMonAn(id){
   
    batLoading();
    axios({
        url: `${BASE_URL}/food/${id}`,
        method: "DELETE",
    })
    .then(function(res){
        tatLoading();
        
        fetchFoodList();
    })
    .catch(function (err){
        tatLoading();
        console.log("🚀 ~ file: main.js:56 ~ xoaMonAn ~ err", err);
        
    })
}
function themMonAn(){
   var monAn = layThongTinTuForm();
   axios({
    url:`${BASE_URL}/food`,
    method: "POST",
    data: monAn,

   }).then(function ( res){
    fetchFoodList();
   })
   .catch(function (err){
    console.log("🚀 ~ file: main.js:85 ~ themMonAn ~ err", err);
    
   });
  
}

function suaMonAn(id){
    batLoading();
axios({
    url: `${BASE_URL}/food/${id}`,
    method: "GET",
})
.then(function (res){
    tatLoading();
    console.log("🚀 ~ file: main.js:97 ~ res", res);
    
})
.catch(function (err){
    tatLoading();
    console.log("🚀 ~ file: main.js:101 ~ suaMonAn ~ err", err);
    
});
}
function capNhatMonAn(){
    var monAn = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/food/${monAn.maMon}`,
        method: "PUT",
        data: monAn,
    });
}
